module BatailleNavaleCore

type Navire = 
    | PorteAvion
    | Croiseur
    | ContreTorpilleur
    | SousMarin
    | Torpilleur

type Direction = 
    | Verticale
    | Horizontale

type Dimension = 
    {
        largeur: int
        hauteur: int }

let longueur = 
    function 
    | PorteAvion -> 5
    | Croiseur -> 4
    | ContreTorpilleur -> 3
    | SousMarin -> 3    
    | Torpilleur -> 2

type Lettre =
    | A
    | B
    | C
    | D
    | E
    | F
    | G
    | H
    | I
    | J

type Chiffre =
    | Un
    | Deux
    | Trois
    | Quatre
    | Cinq
    | Six
    | Sept
    | Huit
    | Neuf
    | Dix

type Coordonnée = Lettre*Chiffre

type PositionNavire = 
    { navire : Navire
      coordonnée : Coordonnée
      direction : Direction }

type Grille = 
    { positionNavires : PositionNavire list }

let placerUnNavire grille positionNavire = 
    { 
        grille with positionNavires = positionNavire :: grille.positionNavires 
    }
