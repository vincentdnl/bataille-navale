module BatailleNavaleCore.Tests

open Expecto

let grille = {
  positionNavires=[];
}

[<Tests>]
let tests =
  testList "Bataille Navale" [    
    testList "placements" [
      testCase "On peut placer un Navire sur la grille et qui ne dépasse pas" <| fun _ ->
        let direction = Verticale
        let coordonnée = (A, Un)
        
        let positionPorteAvion = {
          navire=PorteAvion;
          coordonnée=coordonnée;
          direction=direction;
        } 
        
        let nouvelleGrille = placerUnNavire grille positionPorteAvion
        
        Expect.contains nouvelleGrille.positionNavires positionPorteAvion "La grille ne contient pas le porteAvion"
      
      testCase "On peut placer un Navire sur la grille et qui dépasse de la grille" <| fun _ ->
        let direction = Verticale
        let coordonnée = (J, Dix)
        
        let positionPorteAvion = {
          navire=PorteAvion;
          coordonnée=coordonnée;
          direction=direction;
        } 
        
        let nouvelleGrille = placerUnNavire grille positionPorteAvion
        
        Expect.isNone nouvelleGrille "La grille ne contient pas le porteAvion"  
    ]
  ]
